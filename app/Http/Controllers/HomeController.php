<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stats;
use Illuminate\Support\Facades\Route;
use \Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function add_record()
    {
        return view('add_record');
    }

    public function add_record_store(Request $request)
    {
        if(!$request->classroom)
            return "Debes seleccionar una clase.";
        $path = $request->file('csv_file')->getRealPath();
        $data = array_map('str_getcsv', file($path));
        $csv_data = array_slice($data, 0, 2);


        $count = 0;
        foreach ($data as $d) {
            if($count == 1) {
                if(strlen($d[1]) > 1) {
                    $tmp = new Stats;
                    $tmp->aula          = $request->classroom;
                    $tmp->matricula     = $d[2];
                    $tmp->nombre        = $d[1];
                    $tmp->fecha_inicio  = $d[3];
                    $tmp->fecha_fin     = $d[4];
                    $tmp->save();
                }
            }
            else {
                $count++;
            }
        }
        return redirect()->back()->with('message', 'Has agregado la bitacora satisfactoriamente en la sección de '.$request->classroom);
    }

    public function destroy_record($classroom, $id)
    {
        $record = Stats::where(['aula' => $classroom, 'id' => $id])->delete();
        return "OK";
        //return redirect()->back()->with('message', 'Has eliminado un registro satisfactoriamente.');
    }

    public function lpa2(Request $request)
    {
        $classroom = Route::getFacadeRoot()->current()->uri();

        if($request->search_by && $request->search_filter) {
            if($request->search_by == "period") {
                $year = substr($request->search_filter, 0, 4);
                $period = substr($request->search_filter, -1);
                if($period == 1) {
                    $history = Stats::where('aula', $classroom)->where('fecha_inicio', '>=', $year.'-01-01')->where('fecha_fin', '<', $year.'-07-01')->get();

                    $total_session = 0;
                    foreach ($history as $person) { // Calcular tiempo de sesion
                        $startTime  = Carbon::parse($person->fecha_inicio);
                        $finishTime = Carbon::parse($person->fecha_fin);

                        $totalDuration = $finishTime->diffForHumans($startTime, true, false, 2);
                        $person->session_time = $totalDuration;

                        $total_session += $finishTime->diffInMinutes($startTime);
                    }
                    $total_session /= 60;

                    $history_mouth['january']      = $history->where('fecha_inicio', '>=', $year.'-01-01')->where('fecha_fin', '<',  $year.'-02-01');
                    $history_mouth['february']     = $history->where('fecha_inicio', '>=', $year.'-02-01')->where('fecha_fin', '<',  $year.'-03-01');
                    $history_mouth['march']        = $history->where('fecha_inicio', '>=', $year.'-03-01')->where('fecha_fin', '<',  $year.'-04-01');
                    $history_mouth['april']        = $history->where('fecha_inicio', '>=', $year.'-04-01')->where('fecha_fin', '<',  $year.'-05-01');
                    $history_mouth['may']          = $history->where('fecha_inicio', '>=', $year.'-05-01')->where('fecha_fin', '<',  $year.'-06-01');
                    $history_mouth['june']         = $history->where('fecha_inicio', '>=', $year.'-06-01')->where('fecha_fin', '<',  $year.'-07-01');
                    $history_mouth['july']         = $history->where('fecha_inicio', '>=', $year.'-07-01')->where('fecha_fin', '<',  $year.'-08-01');
                } elseif($period == 2) {
                    $history = Stats::where('aula', $classroom)->where('fecha_inicio', '>=', $year.'-08-01')->where('fecha_fin', '<=', $year.'-12-31')->get();

                    $total_session = 0;
                    foreach ($history as $person) { // Calcular tiempo de sesion
                        $startTime  = Carbon::parse($person->fecha_inicio);
                        $finishTime = Carbon::parse($person->fecha_fin);

                        $totalDuration = $finishTime->diffForHumans($startTime, true, false, 2);
                        $person->session_time = $totalDuration;

                        $total_session += $finishTime->diffInMinutes($startTime);
                    }
                    $total_session /= 60;

                    $history_mouth['august']       = $history->where('fecha_inicio', '>=', $year.'-08-01')->where('fecha_fin', '<',  $year.'-09-01');
                    $history_mouth['september']    = $history->where('fecha_inicio', '>=', $year.'-09-01')->where('fecha_fin', '<',  $year.'-10-01');
                    $history_mouth['octuber']      = $history->where('fecha_inicio', '>=', $year.'-10-01')->where('fecha_fin', '<',  $year.'-11-01');
                    $history_mouth['november']     = $history->where('fecha_inicio', '>=', $year.'-11-01')->where('fecha_fin', '<',  $year.'-12-01');
                    $history_mouth['december']     = $history->where('fecha_inicio', '>=', $year.'-12-01')->where('fecha_fin', '<',  ($year+1).'-01-01');
                }
                return view('lpa2')->with([
                    'history' => $history,
                    'resultType' => $request->search_by,
                    'period' => $period,
                    'history_mounth' => $history_mouth,
                    'total_session' => $total_session,
                    'classroom' => $classroom
                ]);
            } elseif($request->search_by == "year") {
                $history = Stats::where('aula', $classroom)->where('fecha_inicio', '>=', $request->search_filter.'-01-01')->where('fecha_fin', '<', ($request->search_filter+1).'-01-01')->get();

                $total_session = 0;
                foreach ($history as $person) { // Calcular tiempo de sesion
                    $startTime  = Carbon::parse($person->fecha_inicio);
                    $finishTime = Carbon::parse($person->fecha_fin);

                    $totalDuration = $finishTime->diffForHumans($startTime, true, false, 2);
                    $person->session_time = $totalDuration;

                    $total_session += $finishTime->diffInMinutes($startTime);
                }
                $total_session /= 60;

                $history_mouth['january']      = $history->where('fecha_inicio', '>=', $request->search_filter.'-01-01')->where('fecha_fin', '<',  $request->search_filter.'-02-01');
                $history_mouth['february']     = $history->where('fecha_inicio', '>=', $request->search_filter.'-02-01')->where('fecha_fin', '<',  $request->search_filter.'-03-01');
                $history_mouth['march']        = $history->where('fecha_inicio', '>=', $request->search_filter.'-03-01')->where('fecha_fin', '<',  $request->search_filter.'-04-01');
                $history_mouth['april']        = $history->where('fecha_inicio', '>=', $request->search_filter.'-04-01')->where('fecha_fin', '<',  $request->search_filter.'-05-01');
                $history_mouth['may']          = $history->where('fecha_inicio', '>=', $request->search_filter.'-05-01')->where('fecha_fin', '<',  $request->search_filter.'-06-01');
                $history_mouth['june']         = $history->where('fecha_inicio', '>=', $request->search_filter.'-06-01')->where('fecha_fin', '<',  $request->search_filter.'-07-01');
                $history_mouth['july']         = $history->where('fecha_inicio', '>=', $request->search_filter.'-07-01')->where('fecha_fin', '<',  $request->search_filter.'-08-01');
                $history_mouth['august']       = $history->where('fecha_inicio', '>=', $request->search_filter.'-08-01')->where('fecha_fin', '<',  $request->search_filter.'-09-01');
                $history_mouth['september']    = $history->where('fecha_inicio', '>=', $request->search_filter.'-09-01')->where('fecha_fin', '<',  $request->search_filter.'-10-01');
                $history_mouth['octuber']      = $history->where('fecha_inicio', '>=', $request->search_filter.'-10-01')->where('fecha_fin', '<',  $request->search_filter.'-11-01');
                $history_mouth['november']     = $history->where('fecha_inicio', '>=', $request->search_filter.'-11-01')->where('fecha_fin', '<',  $request->search_filter.'-12-01');
                $history_mouth['december']     = $history->where('fecha_inicio', '>=', $request->search_filter.'-12-01')->where('fecha_fin', '<',  ($request->search_filter+1).'-01-01');

                return view('lpa2')->with([
                    'history' => $history,
                    'resultType' => $request->search_by,
                    'history_mounth' => $history_mouth,
                    'total_session' => $total_session,
                    'classroom' => $classroom
                ]);
            }
        } else {
            /*$history = Stats::where('aula', $classroom)->where('fecha_inicio', '>=', Carbon::now()->startOfMonth())->where('fecha_fin', '<=', Carbon::now())->get();

            $total_session = 0;
            foreach ($history as $person) { // Calcular tiempo de sesion
                $startTime  = Carbon::parse($person->fecha_inicio);
                $finishTime = Carbon::parse($person->fecha_fin);

                $totalDuration = $finishTime->diffForHumans($startTime, true, false, 2);
                $person->session_time = $totalDuration;

                $total_session += $finishTime->diffInMinutes($startTime);
            }
            $total_session /= 60;*/

            return view('lpa2')->with([
                /*'history' => $history,
                'resultType' => "mouth",
                'total_session' => $total_session,*/
                'classroom' => $classroom
            ]);
        }
    }

    public function add_log()
    {
        return view('add_record_log');
    }

    public function add_log_upload(Request $request)
    {
        $logon = array_map('str_getcsv', file($request->file('csv_file_logon')->getRealPath())); // LogON
        $logoff = array_map('str_getcsv', file($request->file('csv_file_logoff')->getRealPath())); // LogOFF

        foreach ($logon as $a) {
            $user_on        = $a[0];
            $machine_on     = $a[1];
            $date_on        = substr($a[2], -10);
            $time_on        = $a[3];
            $found_match    = false;

            foreach ($logoff as $b) {
                $user_off       = $b[0];
                $machine_off    = $b[1];
                $date_off       = substr($b[2], -10);
                $time_off       = $b[3];

                if($user_on == $user_off && $machine_on == $machine_off && $date_on == $date_off) { // Si coincide los datos del inicio de sesion con el de cierre de sesion en el mismo dia...
                    $time_start = Carbon::createFromFormat('d/m/Y H:i:s', $date_on." ".substr($time_on, 0, -4))->format('Y-m-d H:i');
                    $time_end   = Carbon::createFromFormat('d/m/Y H:i:s', $date_off." ".substr($time_off, 0, -4))->format('Y-m-d H:i');
                    if($time_start < $time_end) {
                        $found_match = true;
                        break;
                    }
                }
                if($found_match)
                    break;
            }
            //if(!$found_match) echo "SIN COINCIDENCIA, ";

            if($found_match) {
                $tmp = new Stats;
                $tmp->aula          = $request->classroom;
                $tmp->matricula     = $user_on;
                $tmp->nombre        = "-";
                $tmp->fecha_inicio  = $time_start;
                $tmp->fecha_fin     = $time_end;
                $tmp->save();
            }

        }
        return redirect()->back()->with('message', 'Has agregado la bitacora satisfactoriamente en la sección de '.$request->classroom);
    }
}
