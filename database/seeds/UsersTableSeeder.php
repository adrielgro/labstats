<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
                'name' => "Adriel Guerrero",
                'email' =>'adriel.guerrero@uabc.edu.mx',
                'password' => bcrypt('secretpass2018'),
            ],
            [
                'name' => "Adrian Enciso",
                'email' =>'aenciso@uabc.edu.mx',
                'password' => bcrypt('secretpass2018'),
            ],
        ));
    }
}
