<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');


    Route::get('/equipada', 'HomeController@lpa2')->name('equipada');
    Route::post('/equipada', 'HomeController@lpa2')->name('equipada_search');

    Route::get('/ld1', 'HomeController@lpa2')->name('ld1');
    Route::post('/ld1', 'HomeController@lpa2')->name('ld1_search');

    Route::get('/ld2', 'HomeController@lpa2')->name('ld2');
    Route::post('/ld2', 'HomeController@lpa2')->name('ld2_search');

    Route::get('/lpa2', 'HomeController@lpa2')->name('lpa2');
    Route::post('/lpa2', 'HomeController@lpa2')->name('lpa2_search');

    Route::get('/mac', 'HomeController@lpa2')->name('mac');
    Route::post('/mac', 'HomeController@lpa2')->name('mac_search');

    Route::get('/add', 'HomeController@add_record')->name('add_record');
    Route::post('/add', 'HomeController@add_record_store')->name('add_record_store');

    Route::get('/addlog', 'HomeController@add_log')->name('add_log');
    Route::post('/addlog/upload', 'HomeController@add_log_upload')->name('add_log_store');

    Route::get('/destroy/{classroom}/{id}', 'HomeController@destroy_record')->name('destroy_record');
});
