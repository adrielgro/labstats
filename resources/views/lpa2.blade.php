@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card" style="padding: 0 10px">
                    <form class="row" action="{{ $classroom }}" method="POST"  style="padding-top: 15px">
                        @csrf

                        <div class="col s5">
                            <div class="input-field col s12">
                                <select id="search_by" name="search_by">
                                    <option value="" disabled selected>Selecciona una opción</option>
                                    <option value="period">Periodo</option>
                                    <option value="year">Año</option>
                                </select>
                                <label>Busqueda por...</label>
                            </div>
                        </div>
                        <div class="col s5" id="option_filter_period" style="display: none;">
                            <div class="input-field col s12">
                                <select name="search_filter">
                                    <option value="" disabled selected>Selecciona una opción</option>
                                    <option value="2018-1">2018-1</option>
                                    <option value="2018-2">2018-2</option>
                                    <option value="2019-1">2019-1</option>
                                    <option value="2019-2">2019-2</option>
                                </select>
                                <label>Periodo</label>
                            </div>
                        </div>
                        <div class="col s5" class="col s5" id="option_filter_year" style="display: none;">
                            <div class="input-field col s12">
                                <select name="search_filter">
                                    <option value="" disabled selected>Selecciona una opción</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                </select>
                                <label>Año</label>
                            </div>
                        </div>
                        <div class="col s2">
                            <button type="submit" class="btn" style="margin-top: 20px">Buscar</button>
                        </div>
                    </form>
                </div>
            </div>
            @if(isset($history))
                <div class="col s12">
                    <canvas id="myChart" width="400" height="200" style="max-height: 600px"></canvas>
                </div>
            @endif
            <div class="col s12">
                <div class="card">
                    <div class="card-content" style="padding: 10px 16px; border-bottom: 1px solid #c6c6c6">
                        <span class="card-title activator grey-text text-darken-4">Historial | Aula {{ strtoupper($classroom) }}</span>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th>Matrícula</th>
                                <th>Nombre</th>
                                <th>Entrada</th>
                                <th>Salida</th>
                                <th>Tiempo de instancia</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(isset($history) && count($history) > 0)
                                @foreach ($history as $record)
                                    <tr>
                                        <td>{{ $record->matricula }}</td>
                                        <td>{{ $record->nombre }}</td>
                                        <td>{{ $record->fecha_inicio }}</td>
                                        <td>{{ $record->fecha_fin }}</td>
                                        <td>{{ $record->session_time }}</td>
                                        <td><button class="deleteRecord" data-classroom="{{ $classroom }}" data-id="{{ $record->id }}">Borrar</button></td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total: {{ $total_session }} horas</td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="6" class="center-align">Ningun registro</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">

        $(document).ready(function(){
            $('select').formSelect();
        });

        $("#search_by").on('change', function() {
            if($('#search_by').val() == "period") {
                $('#option_filter_year').hide();
                $('#option_filter_period').show();
            } else if($('#search_by').val() == "year") {
                $('#option_filter_year').show();
                $('#option_filter_period').hide();
            }

        });
    </script>
    @if(isset($resultType))
        @if($resultType == "year")
            <script type="text/javascript">
                var ctx = document.getElementById("myChart").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                        datasets: [{
                            label: 'Tiempo de Sesiones',
                            data: [
                                {{ count($history_mounth['january']) }},
                                {{ count($history_mounth['february']) }},
                                {{ count($history_mounth['march']) }},
                                {{ count($history_mounth['april']) }},
                                {{ count($history_mounth['may']) }},
                                {{ count($history_mounth['june']) }},
                                {{ count($history_mounth['july']) }},
                                {{ count($history_mounth['august']) }},
                                {{ count($history_mounth['september']) }},
                                {{ count($history_mounth['octuber']) }},
                                {{ count($history_mounth['november']) }},
                                {{ count($history_mounth['december']) }},
                            ],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            </script>
        @elseif($resultType == "period")
            <script type="text/javascript">
                var ctx = document.getElementById("myChart").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        @if($period == 1)
                            labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio"],
                        @else
                            labels: ["Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                        @endif
                        datasets: [{
                            label: 'Tiempo de Sesiones',
                            @if($period == 1)
                                data: [
                                    {{ count($history_mounth['january']) }},
                                    {{ count($history_mounth['february']) }},
                                    {{ count($history_mounth['march']) }},
                                    {{ count($history_mounth['april']) }},
                                    {{ count($history_mounth['may']) }},
                                    {{ count($history_mounth['june']) }},
                                    {{ count($history_mounth['july']) }},
                                ],
                            @else
                            data: [
                                {{ count($history_mounth['august']) }},
                                {{ count($history_mounth['september']) }},
                                {{ count($history_mounth['octuber']) }},
                                {{ count($history_mounth['november']) }},
                                {{ count($history_mounth['december']) }},
                            ],
                            @endif
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            </script>
        @elseif($resultType == "mouth")

        @endif
    @endif


    <script>
         jQuery(document).ready(function(){
            jQuery('.deleteRecord').click(function(e){
                $(this).parent().parent().remove()
                var classroom = $(this).data('classroom');
                var id = $(this).data('id');
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "destroy/"+classroom+"/"+id,
                  method: 'get',
                  data: {

                  },
                  success: function(result){
                     console.log(result);
                 }});
               });
            });
</script>

@endsection
