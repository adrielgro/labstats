@extends('layouts.app')

@section('content')
    <div class="container" style="max-width: 500px">
        <div class="row">
            <div class="col s12">
                <form action="{{ route('add_record_store') }}" method="POST" enctype="multipart/form-data" class="card center-align" style="padding: 20px">
                    @csrf
                    <p>Importar bitácora</p>
                    <div class="input-field col s12">
                        <select name="classroom">
                            <option value="" disabled selected>Selecciona una opción</option>
                            <option value="equipada">Equipada</option>
                            <option value="ld1">LD1</option>
                            <option value="ld2">LD2</option>
                            <option value="lpa2">LPA2</option>
                            <option value="mac">MAC</option>
                        </select>
                        <label>Aula</label>
                    </div>
                    <input type="file" name="csv_file" />
                    <br>
                    <br>
                    <button type="submit" class="btn button">Enviar</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        $(document).ready(function(){
            $('select').formSelect();
        });
    </script>
@endsection
