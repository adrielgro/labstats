@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content" style="padding: 10px 16px; border-bottom: 1px solid #c6c6c6">
                        <span class="card-title activator grey-text text-darken-4">Historial | Aula equipada<i class="material-icons right">search</i></span>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th>Matrícula</th>
                                <th>Nombre</th>
                                <th>Entrada</th>
                                <th>Salida</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(count($history) > 0)
                                @foreach ($history as $record)
                                    <tr>
                                        <td>{{ $record->matricula }}</td>
                                        <td>{{ $record->nombre }}</td>
                                        <td>{{ $record->fecha_inicio }}</td>
                                        <td>{{ $record->fecha_fin }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="center-align">Ningun registro</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
