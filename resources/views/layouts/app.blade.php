<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Charset UTF-8 -->
        <meta charset="utf-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <!-- Styles -->
        <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet"  media="screen,projection">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Title -->
        <title>{{ config('app.name', 'Laravel') }}</title>

        <style media="screen">
            body {
                background-color: #f6f6f6;
            }
        </style>
    </head>

    <body>
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="{{ route('add_record') }}">Añadir registro personalizado</a></li>
            <li><a href="{{ route('add_log') }}">Añadir registro logon</a></li>
            <li class="divider"></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </ul>
        <nav class="green darken-2">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo">{{ config('app.name', 'Laravel') }}</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down" style="margin-right: 30px;">
                    <li><a href="{{ route('home') }}">Resumen</a></li>
                    <li><a href="{{ route('equipada') }}">Equipada</a></li>
                    <li><a href="{{ route('ld1') }}">LD1</a></li>
                    <li><a href="{{ route('ld2') }}">LD2</a></li>
                    <li><a href="{{ route('lpa2') }}">LPA2</a></li>
                    <li><a href="{{ route('mac') }}">MAC</a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Opciones<i class="material-icons right">arrow_drop_down</i></a></li>
                </ul>
            </div>
        </nav>
        <main style="margin-top: 30px">
            @yield('content')
        </main>

        <!-- Scripts -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
        <script src="{{ asset('js/app.js') }}"></script>

        </script>
        <script type="text/javascript">
             $('.dropdown-trigger').dropdown({constrainWidth: false });

             @if(session()->has('message'))
                 M.toast({html: '{{ session()->get('message') }}'})
             @endif

        </script>
        @yield('footer')
    </body>
</html>
